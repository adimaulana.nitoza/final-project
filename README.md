## Final Project

## Kelompok 28

## Anggota Kelompok

Adi Maulana (@adimaulana_id) <br>
Muhammad Abid Abdullah (@abidabdullah)

## Tema Project 

Portal Berita

## ERD

<p align="center"><a href="#" target="_blank"><img src="erd_project_1.png" width="400"></a></p>

## Link Vidio

Link Demo : https://youtu.be/lT3wopmNxog

Link Deploy(Optional) : http://final-project-berita.herokuapp.com/
