@extends('layout.master')

@section('judul')
    Halaman Create Berita
@endsection

@push('style')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-rc.0/css/select2.min.css" integrity="sha512-aD9ophpFQ61nFZP6hXYu4Q/b/USW7rpLCQLX6Bi0WJHXNO7Js/fUENpBQf/+P4NtpzNX0jSgR5zVvPOJp+W2Kg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-rc.0/js/select2.full.min.js" integrity="sha512-PZUUFofP00wI366Au6XSNyN4Zg8M8Kma4JKIG7ywt8FEY1+Ur0H+FAlH6o0fKoCrdmM4+ZzMyW30msp8Z2zDaA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endpush

@push('script')
   <script src="https://cdn.tiny.cloud/1/dc2k354qxxarp30rn9afmlhn7vua7cpkx6e2odc3mexo8hlz/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
   <script>
    tinymce.init({
      selector: 'textarea',
    });
  </script>
  <script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
      });
  </script>
@endpush


@section('content')

    <form action="/berita" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label >Judul Berita</label>
        <input type="text" class="form-control" name="judul" >
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <div class="form-group">
        <label >Gambar</label>
        <input type="file" name="gambar" class="form-control">
     </div>
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <div class="form-group">
        <label >Content</label>
        <textarea type="text" class="form-control" name="kontent" cols="30" rows="10"></textarea>
    </div>
    @error('kontent')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <div>
        <label >Kategori</label>
        <select name="kategori_id" class="form-control js-example-basic-single" id="mySelect2">
            <option value="">---Pilih Kategori---</option>
            @foreach ($kategori as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    @error('kategori_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>

    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <br><br><br>

@endsection
