@extends('layout.master')

@section('judul')
    Halaman Edit Berita
@endsection

@push('style')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
   <script src="https://cdn.tiny.cloud/1/dc2k354qxxarp30rn9afmlhn7vua7cpkx6e2odc3mexo8hlz/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
   <script>
    tinymce.init({
      selector: 'textarea',
    });
  </script>
  <script>
    $(document).ready(function() {
      $('.js-example-basic-single').select2();
    });
  </script>
@endpush

@section('content')

    <form action="/berita/{{$berita->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label >Judul Berita</label>
        <input type="text" class="form-control" name="judul" value="{{$berita->judul}}">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <div class="form-group">
        <label >Gambar</label>
        <input type="file" name="gambar" class="form-control">
     </div>
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <div class="form-group">
        <label >Content</label>
        <textarea type="text" class="form-control" name="kontent" cols="30" rows="10">{{$berita->kontent}}</textarea>
    </div>
    @error('kontent')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <div class="form-group">
        <label >Kategori</label>
        <select name="kategori_id" id="" class="js-example-basic-single">
            <option value="">---Pilih Kategori---</option>
            @foreach ($kategori as $item)
                @if ($item->id === $berita->kategori_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                <option value="{{$item->id}}" >{{$item->nama}}</option>
                @endif
            @endforeach
        </select>
    </div>
    @error('kategori_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>

    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <br><br><br>

@endsection
