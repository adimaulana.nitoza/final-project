@extends('layout.master')

@section('judul')
    Halaman Berita
@endsection

@section('content')
@auth
<a href="/berita/create" class="btn btn-success mb-3">Tambah</a>
@endauth
<div class="row">
    @forelse ($berita as $item)
        <div class="col-lg-6">
            <!-- Blog post-->
            <div class="card mb-4">
                <a href="#!"><img class="card-img-top" src="{{asset('gambar/'. $item->gambar)}}" alt="..." /></a>
                <div class="card-body">
                    <a class="badge bg-secondary text-decoration-none link-light mb-3" href="/kategori/{{$item->kategori->id}}">{{$item->kategori->nama}}</a>
                    {{-- <div class="small text-muted">January 1, 2021</div> --}}
                    <h2 class="card-title h4">{{$item->judul}}</h2>
                    <p class="card-text"> {!!Str::limit($item->kontent, 30)!!} </p>

                    <form action="/berita/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a class="btn btn-primary btn-sm" href="/berita/{{$item->id}}">Detail</a>
                        @auth
                        <a class="btn btn-warning btn-sm" href="/berita/{{$item->id}}/edit">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        @endauth
                    </form>
                </div>
            </div>
        </div>
    @empty
        <h4>Belum Ada Data Berita</h4>
    @endforelse

</div>

@endsection
