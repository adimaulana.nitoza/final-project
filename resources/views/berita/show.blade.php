@extends('layout.master')

@section('judul')
    Halaman Detail Berita {{$berita->judul}}
@endsection

@push('script')
   <script src="https://cdn.tiny.cloud/1/dc2k354qxxarp30rn9afmlhn7vua7cpkx6e2odc3mexo8hlz/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
   <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section('content')


    <!-- Post content-->
    <article>
        <!-- Post header-->
        <header class="mb-4">
            <!-- Post title-->
            <h1 class="fw-bolder mb-1">{{$berita->judul}}</h1>
            <!-- Post meta content-->
            <div class="text-muted fst-italic mb-2">{{$berita->created_at}}</div>
            <!-- Post categories-->
            <a class="badge bg-secondary text-decoration-none link-light" href="/kategori/{{$berita->kategori->id}}">{{$berita->kategori->nama}}</a>
            {{-- <a class="badge bg-secondary text-decoration-none link-light" href="#!">Freebies</a> --}}
        </header>
        <!-- Preview image figure-->
        <figure class="mb-4"><img class="img-fluid rounded" src="{{asset('gambar/'. $berita->gambar)}}" alt="..."></figure>
        <!-- Post content-->
        <section class="mb-5">
            <p class="fs-5 mb-4">{!!$berita->kontent!!}</p>

        </section>
    </article>
    <a class="btn btn-secondary btn-sm" href="/berita">Kembali</a>
    <br><br><br><br><br>
    <!-- Comments section-->
    <section class="mb-5">
        <div class="card bg-light">
            <div class="card-body">
                <!-- Comment form-->
                <form action="/komentar" method="POST" enctype="multipart/form-data" class="mb-4">
                  @csrf
                  <div class="form-group">
                    <input type="hidden" name="berita_id" value="{{$berita->id}}">
                    <textarea name="isi" class="form-control" rows="3" placeholder="Tinggalkan Komentar ..."></textarea>
                  </div>
                  @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <br>
                  <button type="submit" class="btn btn-primary">Kirim</button>
                </form>
                <!-- Comment with nested comments-->
                <div class="d-flex mb-4">
                  <!-- Single comment-->
                @foreach ($berita->komentar as $item)
                <div class="d-flex">
                    <div class="ms-3">
                        <div class="fw-bold">{{$item->user->name}}</div>
                        {!!$item->isi!!}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>



@endsection
