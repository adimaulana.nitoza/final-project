@extends('layout.master')

@section('judul')
    Halaman Profile
@endsection

@push('script')
   <script src="https://cdn.tiny.cloud/1/dc2k354qxxarp30rn9afmlhn7vua7cpkx6e2odc3mexo8hlz/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
   <script>
    tinymce.init({
      selector: 'textarea',
    });
  </script>
@endpush

@section('content')

<form action="/profile/{{$profile->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
      <label>Nama</label>
      <input type="text" value="{{$profile->user->name}}" class="form-control" disabled>
  </div>
  <div class="form-group">
      <label>Email</label>
      <input type="text" value="{{$profile->user->email}}" class="form-control" disabled>
  </div>
  <div class="form-group">
      <label >Alamat</label>
      <textarea type="text" class="form-control" name="alamat" cols="30" rows="10">{{$profile->alamat}}</textarea>
  </div>
  <div class="form-group">
      <label >Bio</label>
      <textarea type="text" class="form-control" name="bio" cols="30" rows="10">{{$profile->bio}}</textarea>
  </div>
  @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <br>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

    <br><br><br><br> <br><br><br><br>

@endsection
