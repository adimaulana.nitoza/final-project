@extends('layout.master')

@section('judul')
    Halaman  Kategori
@endsection

@push('script')
   <script src="https://cdn.tiny.cloud/1/dc2k354qxxarp30rn9afmlhn7vua7cpkx6e2odc3mexo8hlz/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
   <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section('content')
@auth
<a href="/kategori/create" class="btn btn-success mb-3">Tambah</a>
@endauth
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">List Berita</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $key => $item)
      <tr>
        <td>{{ $key + 1}}</td>
        <td>{{ $item->nama }}</td>
        <td>
          <ul>
              @foreach ($item->berita as $value)
                <li>{{$value->judul}}</li>
              @endforeach
          </ul>
        </td>
        <td>
            <form action="/kategori/{{$item->id}}" method="POST">
                @method('delete')
                @csrf

                <a href="/kategori/{{$item->id}}" class="btn btn-secondary btn-sm">Detail</a>
                @auth
                <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                @endauth
            </form>
        </td>
      </tr>
      @empty
                <tr>
                    <td>Data Masih Kosong</td>
                </tr>
            @endforelse

    </tbody>
  </table>
  <br><br><br><br> <br><br><br><br>

@endsection
