@extends('layout.master')

@section('judul')
    Halaman Detail Kategori {{$kategori->nama}}
@endsection


@section('content')

    <h3>Nama : {{$kategori->nama}}</h3>
    <br>
    <h4>Deskripsi :</h4>
    <div>
      <!DOCTYPE html>
      <html lang="en" dir="ltr">
        <head>
          <meta charset="utf-8">
          <title></title>
        </head>
        <body>

        </body>
      </html>
      {!!$kategori->deskripsi!!}
    </div>

    <div class="row">
        @foreach ($kategori->berita as $item)
            <div class="col-4">
                <!-- Blog post-->
                <div class="card mb-4">
                    <a href="#!"><img class="card-img-top" src="{{asset('gambar/'. $item->gambar)}}" alt="..."></a>
                    <div class="card-body">
                        <h2 class="card-title h4">{{$item->judul}}</h2>
                        <p class="card-text"> {!!Str::limit($item->kontent, 30)!!} </p>
                        <a class="btn btn-primary btn-sm" href="/berita/{{$item->id}}">Detail</a>
                    </div>
                </div>
            </div>
        @endforeach

    </div>

    <br><br><br><br> <br><br><br><br>

@endsection
