@extends('layout.master')

@section('judul')
    Halaman Create Kategori
@endsection

@push('script')
  <script src="https://cdn.tiny.cloud/1/dc2k354qxxarp30rn9afmlhn7vua7cpkx6e2odc3mexo8hlz/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
      tinymce.init({
        selector: '#mytextarea'
      });
    </script>
@endpush

@section('content')

    <form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
        <label >Nama Kategori</label>
        <input type="text" class="form-control" name="nama" >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <div class="form-group">
        <label >Deskripsi</label>
        <textarea id="mytextarea" type="text" class="form-control" name="deskripsi" cols="30" rows="10"></textarea>

    </div>
    @error('deskripsi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <br><br><br>

@endsection
