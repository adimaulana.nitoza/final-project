@extends('layout.master')

@section('judul')
    Halaman Index
@endsection

@section('content')

<div class="carousel slide mb-5" data-bs-ride="carousel">
    <div class="carousel-inner">
      @forelse ($berita as $item)
        @if ($loop->first)
        <div class="carousel-item active">
          <img src="{{asset('gambar/'. $item->gambar)}}" class="d-block w-100" alt="...">
          <div class="carousel-caption d-none d-md-block bg-dark">
            <h5>{{$item->judul}}</h5>
            <p>{!!Str::limit($item->kontent, 30)!!}</p>
            <a href="/berita/{{$item->id}}">Selengkapnya...</a>
          </div>
        </div>
        @endif
        <div class="carousel-item">
          <img src="{{asset('gambar/'. $item->gambar)}}" class="d-block w-100" alt="...">
          <div class="carousel-caption d-none d-md-block bg-dark">
            <h5>{{$item->judul}}</h5>
            <p>{!!Str::limit($item->kontent, 30)!!}</p>
            <a href="/berita/{{$item->id}}">Selengkapnya...</a>
          </div>
        </div>
        @empty
        <div class="d-none d-md-block bg-light py-3">
          <h4><center>Belum Ada Berita</center></h4>
        </div>
        @endforelse
    </div>
    <button class="carousel-control-prev bg-dark" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next bg-dark" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
</div>
@endsection
