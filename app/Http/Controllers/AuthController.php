<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;

class AuthController extends Controller
{
    //
    public function register(){

        Alert::success('Berhasil', 'Anda Berhasil Register');
        return view('page.form');
    }

    public function dashboard(Request $request){
        // dd($request->all());
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('page.welcome', compact('fname', 'lname'));
    }
}
