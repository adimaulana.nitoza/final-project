<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;


use App\Komentar;

class KomentarController extends Controller
{
    public function store(Request $request)
    {
      $request->validate([
          'isi' => 'required',

      ]);
      Alert::error('Error', 'Error Message');

      $komentar = new Komentar;
      $komentar->isi = $request->isi;
      $komentar->users_id = auth::id();
      $komentar->berita_id = $request->berita_id;

      $komentar->save();

      Alert::success('Berhasil', 'Komentar Berhasil Terpublish');
      return redirect()->back();
    }
}
