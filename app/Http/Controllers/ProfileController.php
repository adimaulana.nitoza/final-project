<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;


use App\Profile;

class ProfileController extends Controller
{
  public function index()
  {
      $profile = Profile::where('users_id',auth::id())->first();
      return view('profile.index', compact('profile'));
  }
  public function update(Request $request, $id)
  {
      $request->validate([
          'alamat' => 'required',
          'bio' => 'required'
      ]);

      $profile = Profile::find($id);
      $profile->alamat = $request->alamat;
      $profile->bio = $request->bio;

      $profile->update();

      Alert::success('Berhasil', 'Update Profile Berhasil');
      return redirect('/profile');
  }
}
