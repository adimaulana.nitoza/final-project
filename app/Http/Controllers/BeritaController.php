<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Alert;

use App\Berita;

class BeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::all();
        return view('berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        return view('berita.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'gambar' => 'required|image|mimes:png,jpg,jpeg|max:2048',
            'kontent' => 'required',
            'kategori_id' => 'required'
        ]);

        $NamaGambar = time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('gambar'), $NamaGambar);

        $berita = new Berita;

        $berita->judul = $request->judul;
        $berita->gambar = $NamaGambar;

        $berita->kontent = $request->kontent;
        $berita->kategori_id = $request->kategori_id;

        $berita->save();

        Alert::success('Berhasil', 'Tambah Berita Berhasil');
        return redirect('/berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = DB::table('kategori')->first();
        $berita = Berita::findOrFail($id);
        return view('berita.show', compact('berita', 'kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->get();
        $berita = Berita::findOrFail($id);
        return view('berita.edit', compact('berita', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'gambar' => 'image|mimes:png,jpg,jpeg|max:2048',
            'kontent' => 'required',
            'kategori_id' => 'required'
        ]);

        $berita = Berita::find($id);

        if($request->has('gambar')) {
            $NamaGambar = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('gambar'), $NamaGambar);

            $berita->judul = $request->judul;
            $berita->gambar = $NamaGambar;
            $berita->kontent = $request->kontent;
            $berita->kategori_id = $request->kategori_id;
        } else {
            $berita->judul = $request->judul;
            $berita->kontent = $request->kontent;
            $berita->kategori_id = $request->kategori_id;

        }


        $berita->update();

        Alert::success('Berhasil', 'Edit Berita Berhasil');
        return redirect('/berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::find($id);
        $path = "gambar/";
        File::delete($path . $berita->gambar);
        $berita->delete();
        return redirect('/berita');
    }
}
