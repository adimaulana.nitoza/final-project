<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;

class IndexController extends Controller
{
    //
    public function index(){
        $berita = Berita::all();
        return view('page.index', compact('berita'));
    }
}
